# homer-bot

> D'oh!

A gitlab merge bot far more stupid than [marge-bot](https://github.com/smarkets/marge-bot).

It tries to get some information about a Gitlab project, and evaluate whether the MRs are ready to be merged **for real**.

It is a very dumb and conservative bot: stateless, oneshot (no daemon running).
It is mainly design to monitor merge requests, and not actually merge them.

The only changes it is allowed to perform are:
- comment on a MR
- add/remove a label

## Build

```
go build
```

## Run

The following documentation is assuming you know your gitlab project identifier: just look at the Home page of your project, you should find a `Project ID: XXXXX` item.

Next, you need to generate a valid [Gitlab private token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html). The privileges related to the token need to be large enough to access the Gitlab API and be able to:
* get all members of the project (to check their role in the project)
* get Project's MRs information
* get Project's MRs pipelines information
* get Project's MRs award emojis (for the votes)
* get Project's MRs discussions
* add/remove a label on a Project's MR
* add a comment (`note` in gitlab) in a Project's MR 

Then, you need to provide this token through the `GITLAB_PRIVATE_TOKEN` environment variable.

You can also override the default gitlab API root URL with the `GITLAB_API_ROOT_URL` environment variable.

```shell
export GITLAB_PRIVATE_TOKEN=<your private token here>
export GITLAB_API_ROOT_URL=<your gitlab API root URL> # Default is https://code.videolan.org/api/v4
./homer-bot -project <projectId>
```

## Gitlab CI integration

You can use and manage Homer with the gitlab-ci schedules:
- create a schedule per project you want to monitor
- add the `GITLAB_PRIVATE_TOKEN`, `GITLAB_API_ROOT_URL` (if needed) and `PROJECTID` variables to the schedule environment
- run/cron the schedule!

This way Homer will be 100% managed (and monitored) by gitlab.

## Strategy

The current strategy is described in [this page](doc/acceptance.md).

