package main

import (
	"fmt"
	"regexp"
	"time"
)

// StatusLabels are MR labels that are used to tag a status on the MR
// WARNING: if you add a new Status, do not forget to add it to the AllStatusLabels variable below.
const (
	StatusLabelNotCompliant = StatusLabel("MRStatus::NotCompliant")
	StatusLabelReviewable   = StatusLabel("MRStatus::Reviewable")
	StatusLabelInReview     = StatusLabel("MRStatus::InReview")
	StatusLabelAcceptable   = StatusLabel("MRStatus::Acceptable")
	StatusLabelAccepted     = StatusLabel("MRStatus::Accepted")
)

// AllStatusLabels list all known status labels
// WARNING: if you add a new Status, do not forget to add it to the this slice
var AllStatusLabels []StatusLabel = []StatusLabel{
	StatusLabelNotCompliant,
	StatusLabelReviewable,
	StatusLabelInReview,
	StatusLabelAcceptable,
	StatusLabelAccepted}

// IsStatusLabel checks if the string is part of the Status labels
func IsStatusLabel(s string) bool {
	sLabel := StatusLabel(s)
	for _, l := range AllStatusLabels {
		if l == sLabel {
			return true
		}
	}
	return false
}

// StatusLabel represents a MR status label
type StatusLabel string

// SetStatusLabel change the status label. Returns false if the the label is already found.
func (mr *MergeRequest) SetStatusLabel(label StatusLabel) (bool, error) {
	// remove any status label that is not "label"
	found := false
	toRemove := []string{}
	for _, mrLabel := range mr.GetLabels() {
		if StatusLabel(mrLabel) == label {
			found = true
			continue
		}
		if IsStatusLabel(mrLabel) {
			toRemove = append(toRemove, mrLabel)
		}
	}
	if found && len(toRemove) == 0 {
		logMR(mr.IID, "Status labels are ok, nothing to do")
		return false, nil
	}
	toAdd := []string{}
	if !found {
		toAdd = []string{string(label)}
	}
	logMR(mr.IID, "Changing labels: Adding ", toAdd, ", Removing ", toRemove)
	if err := mr.AddAndRemoveLabels(toAdd, toRemove); err != nil {
		return !found, err
	}
	logMR(mr.IID, "Labels successfully changed")
	return !found, nil
}

// RemoveAllStatusLabels removes all labels considered as status labels
func (mr *MergeRequest) RemoveAllStatusLabels() error {
	toRemove := []string{}
	for _, mrLabel := range mr.GetLabels() {
		if IsStatusLabel(mrLabel) {
			toRemove = append(toRemove, mrLabel)
		}
	}
	if len(toRemove) == 0 {
		logMR(mr.IID, "No status labels, nothing to do")
		return nil
	}
	if err := mr.AddAndRemoveLabels([]string{}, toRemove); err != nil {
		return err
	}
	logMR(mr.IID, "Labels successfully removed")
	return nil
}

// IsWelcomeMessagePresent detects if the bot already wrote a Welcome message
func (mr *MergeRequest) IsWelcomeMessagePresent() (bool, error) {
	notes, err := mr.GetAllNotes(false)
	if err != nil {
		return false, err
	}
	for _, note := range *notes {
		var metadata Metadata
		found, err := retrieveMetadataFromString(note.Body, &metadata)
		if err != nil {
			return false, err
		}
		if found && metadata.MessageType == messageTypeWelcome {
			return true, nil
		}
	}
	return false, nil
}

// GenerateWelcomeMessage a Welcome Message in Markdown
func (mr *MergeRequest) GenerateWelcomeMessage() (string, error) {
	message := fmt.Sprintln("### Thanks")
	message += fmt.Sprintln("Thanks for your contribution!")
	message += fmt.Sprintln("")
	message += fmt.Sprintln("When all the following conditions are fulfilled, your MergeRequest will be reviewed by the Team:")
	message += fmt.Sprintln("- the check pipeline pass")
	message += fmt.Sprintln("- the MR is considered as 'mergeable' by gitlab")
	message += fmt.Sprintln("")
	message += fmt.Sprintln("You can find more details about the acceptance process [here](", acceptanceDocURL, ").")
	message += fmt.Sprintln("")
	// automatic signature
	message += fmt.Sprintln(automaticMessage)
	// Add messagetype in metadata
	meta, err := generateMetadataString(Metadata{MessageType: messageTypeWelcome})
	if err != nil {
		return "", err
	}
	// empty link seems to be equivalent to commenting (not displayed)
	message += fmt.Sprintln("[](" + meta + ")")
	return message, nil
}

// GetAllNotesReferingUser retrieve all notes refering to a specific user in their body
func (mr *MergeRequest) GetAllNotesReferingUser(user User) (*[]Note, error) {
	allNotes, err := mr.GetAllNotes(false)
	if err != nil {
		return allNotes, err
	}
	notes := []Note{}
	keyword := "@" + user.Username
	// as @ is not ascii character, you cannot directly use \b as separator for a word.
	r, err := regexp.Compile(`(?:\A|\z|\s)(` + keyword + `)(?:[[:punct:]]|\s|\z)`)
	if err != nil {
		return nil, err
	}
	for _, note := range *allNotes {
		if r.MatchString(note.Body) {
			notes = append(notes, note)
		}
	}
	return &notes, nil
}

// DevScore computes a Developer Score by checking the emoji awards (aka "thumbs up/down").
// Only users with an access level higher or equal to Developer are taken into account.
// Your own vote does not count.
// Returns:
// - the score
// - the number of votes
// - the last time the score has changed its sign, or Epoch if undefined.
// - an error if any
// strictly == true means the function returns the last time the score has become < 1 or >= 1
// strictly == false means the function returns the last time the score has become < 0 or >= 0
func (mr *MergeRequest) DevScore(strictly bool) (int64, int64, time.Time, error) {
	t := time.Time{} // Zero time
	devs, err := mr.Project.GetDevOrHigherByID()
	if err != nil {
		return 0, 0, t, err
	}
	emojis, err := mr.GetAwardEmojis()
	if err != nil {
		return 0, 0, t, err
	}
	var score int64 = 0
	var votes int64 = 0
	for _, award := range *emojis {
		logMRVerbose(mr.IID, "Emoji ", award.Name, award.UpdatedAt)
		if award.User.ID == mr.Author.ID {
			continue // your own vote does not count
		}
		if _, ok := devs[award.User.ID]; !ok {
			continue // not a dev
		}
		switch emoji := award.Name; emoji {
		case VoteUp:
			score++
			votes++
			if (score == 1 && strictly) || (score == 0 && !strictly) {
				t = award.UpdatedAt
			}
		case VoteDown:
			score--
			votes++
			if (score == 0 && strictly) || (score == -1 && !strictly) {
				t = award.UpdatedAt
			}
		default:
			logMRVerbose(mr.IID, emoji, "is not an emoji that counts as a vote")
		}
		if votes == 1 && t.IsZero() {
			// this is the first vote that count, so update the time even if there was no time change
			t = award.UpdatedAt
		}
	}
	return score, votes, t, nil
}
