# Homer MergeRequest Acceptance process

The Acceptance process slightly differs whether you are a project's *developer* (or higher) or not.

### Voting system

- The **Score** is computed by comparing the number of :thumbsup: and :thumbsdown: given to the MergeRequest
- Only project *developers* (or higher) are taken into account

### Process Diagram

![](acceptance.svg)


# Implementation

Homer uses a simple decision graph to determine the status of the merge requests.

### Status Definitions

Visible status:
- NotCompliant: This MR doesn't pass the mandatory requirements such as a successful pipeline
- Reviewable: This MR has passed the pipeline and can now be reviewed by developers
- InReview: This MR is being reviewed and has opened discussions
- Acceptable: This MR will be accepted once the cooldown period has passed (24h or 72h)
- Accepted: This MR can be rebased and merged by a developer

Internal status:
- Submitted: Initial status
- PipelineReady: Is it possible to check the pipeline result
- QuantumState: It is not possible to know if the merge request is mergeable (the mergeability check is not finished)
- PipelineNotFinished: The pipeline check is not finished.


### If the MergeRequest author has Developer role (or higher)

```mermaid
graph TD
    Submitted([Submitted]) --> Q1{Is it mergeable?}
    Q1 -->|Yes| PipelineReady([PipelineReady])
    Q1 -->|No| NotCompliant([NotCompliant])
    Q1 -->|Cannot determine| QuantumState([QuantumState])
    PipelineReady --> Q2{Is CI OK?}
    Q2 -->|Yes| Reviewable([Reviewable])
    Q2 -->|No| NotCompliant
    Q2 -->|Cannot determine| PipelineNotFinished([PipelineNotFinished])
    Reviewable -->|"No vote and No thread opened during 72h"| Accepted([Accepted])
    Reviewable -->|"Thread opened or another Developer voted"| InReview([InReview])
    InReview -->|"All threads resolved and Score >= 0"| Acceptable([Acceptable])
    Acceptable -->|Wait for 24h| Accepted
classDef default fill:#fff,stroke-width:2px
classDef internal stroke-dasharray: 5 5
class Submitted,PipelineReady,QuantumState,PipelineNotFinished internal
style Acceptable color:#8fbc8f,stroke:#8fbc8f
style Accepted color:#009966,stroke:#009966
style InReview color:#FF8800,stroke:#FF8800
style NotCompliant color:#6699cc,stroke:#6699cc
style Reviewable color:#FF610A,stroke:#FF610A
```

### If the MergeRequest author is **NOT** a project's developer

```mermaid
graph TD
    Submitted([Submitted]) --> Q1{Is it mergeable?}
    Q1 -->|Yes| PipelineReady([PipelineReady])
    Q1 -->|No| NotCompliant([NotCompliant])
    Q1 -->|Cannot determine| QuantumState([QuantumState])
    PipelineReady --> Q2{Is CI OK?}
    Q2 -->|Cannot determine| PipelineNotFinished([PipelineNotFinished])
    Q2 -->|No| NotCompliant
    Q2 -->|Yes| Reviewable([Reviewable])
    Reviewable -->|"Thread opened or a Developer voted"| InReview([InReview])
    InReview -->|"All threads resolved and Score > 0"| Acceptable([Acceptable])
    Acceptable -->|Wait for 72h| Accepted([Accepted])
classDef default fill:#fff,stroke-width:2px
classDef internal stroke-dasharray: 5 5
class Submitted,PipelineReady,QuantumState,PipelineNotFinished internal
style Acceptable color:#8fbc8f,stroke:#8fbc8f
style Accepted color:#009966,stroke:#009966
style InReview color:#FF8800,stroke:#FF8800
style NotCompliant color:#6699cc,stroke:#6699cc
style Reviewable color:#FF610A,stroke:#FF610A
```

