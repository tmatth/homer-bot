package main

import (
	"fmt"
	"time"
)

// The State list should reflect the nodes from the acceptance graphs in `doc/acceptance.md`.
// None state is used in case of errors in the graph traversal
// SchrodingerState/Quantum state is used when homer cannot decide at which state the MR is
const (
	InitialState             = GraphNode("Submitted")
	NotCompliantState        = GraphNode("NotCompliant")
	PipelineNotFinishedState = GraphNode("PipelineNotFinished")
	PipelineReadyState       = GraphNode("PipelineReady")
	ReviewableState          = GraphNode("Reviewable")
	InReviewState            = GraphNode("InReview")
	AcceptableState          = GraphNode("Acceptable")
	AcceptedState            = GraphNode("Accepted")
	SchrodingerState         = GraphNode("QuantumState")
	NoneState                = GraphNode("None")
)

// MaxTransitions represents the maximum number of transitions during graph traversal.
// This is used to avoid infinite loops
const maxTransitions = 100

// GraphNode is a very simple representation of a graph node.
type GraphNode string

// GraphEdge represents an edge/transition in the graph.
// From: edge start (the transition state where the condition will be evaluated)
// Condition: a function that will evaluate if the transition condition is fulfilled.
// To: edge end (this state will be the new current one if the Condition is fulfilled.
// ToOnFalse: if the Condition is not NOT OK and ToOnFalse is equal to NoneState,
//   nothing happens (the next edge is evaluated). Otherwise, the new current state
//   will be changed to ToOnFalse. This is a very simple way to implement a
//   decision-like transition
type GraphEdge struct {
	From      GraphNode
	Condition func(*MergeRequest) Check
	To        GraphNode
	ToOnFalse GraphNode
}

// SimpleEdge defines a simple non-decisional transition (ToOnFalse is set to NoneState)
func SimpleEdge(from GraphNode, condition func(*MergeRequest) Check, to GraphNode) GraphEdge {
	return GraphEdge{from, condition, to, NoneState}
}

// FindMRGraphState computes the Node where the MR should be.
// returns the final GraphNode, and the list of checks that ended to an actual state transition
// if the GraphNode is NoneState, something gone wrong.
func FindMRGraphState(mr *MergeRequest, graph []GraphEdge) (GraphNode, []Check) {
	checks := []Check{}
	state := InitialState
	iteration := 0
	for ; iteration < maxTransitions; iteration++ {
		transit := false
		for _, edge := range graph {
			if edge.From == state {
				logMRVerbose(mr.IID, "found a graph edge for state ", state)
				check := edge.Condition(mr)
				logMR(mr.IID, check.String())
				if check.Error != nil {
					checks = append(checks, check)
					return NoneState, checks
				}
				if check.Success {
					logMRVerbose(mr.IID, "Transition: ", state, " -> ", edge.To)
					checks = append(checks, check)
					state = edge.To
					transit = true
					break
				} else if edge.ToOnFalse != NoneState && state != edge.ToOnFalse {
					logMRVerbose(mr.IID, "Transition: ", state, " -> ", edge.ToOnFalse)
					checks = append(checks, check)
					state = edge.ToOnFalse
					transit = true
					break
				}
			}
		}
		if !transit {
			logMRVerbose(mr.IID, "No transition from state ", state, ", this is the final state")
			break
		}
	}
	if iteration >= maxTransitions {
		// We should never been there: this means your Graph is not deterministic.
		panic("Infinite loop in graph traversal!")
	}
	return state, checks
}

// UserAcceptanceGraph is the actual representation of the Acceptance graph for non-developer users
// Note: transitions will be evaluated in the order of definitions
func UserAcceptanceGraph() []GraphEdge {
	graph := []GraphEdge{}
	graph = append(graph, SimpleEdge(InitialState, CheckStatusNotDecidable, SchrodingerState))
	graph = append(graph, GraphEdge{InitialState, CheckMergeable, PipelineReadyState, NotCompliantState})
	graph = append(graph, SimpleEdge(PipelineReadyState, CheckPipelineIsNotFinished, PipelineNotFinishedState))
	graph = append(graph, GraphEdge{PipelineReadyState, CheckPipelines, ReviewableState, NotCompliantState})
	graph = append(graph, SimpleEdge(ReviewableState, CheckVoteOrThread, InReviewState))
	graph = append(graph, SimpleEdge(InReviewState, CheckAllThreadResolvedAndDevScoreStrict, AcceptableState))
	graph = append(graph, SimpleEdge(AcceptableState, CheckIsNotActiveAnymoreUser, AcceptedState))
	// The other edges (Acceptable -> In Review, Accepted -> In Review, etc.) will never be trigger for now:
	// Homer will never reach the Acceptable/In Review states - votes or discussions will not meet the requirements
	return graph
}

// DeveloperAcceptanceGraph is the actual representation of the Acceptance graph for developer users
// Note: transitions will be evaluated in the order of definitions
func DeveloperAcceptanceGraph() []GraphEdge {
	graph := []GraphEdge{}
	graph = append(graph, SimpleEdge(InitialState, CheckStatusNotDecidable, SchrodingerState))
	graph = append(graph, GraphEdge{InitialState, CheckMergeable, PipelineReadyState, NotCompliantState})
	graph = append(graph, SimpleEdge(PipelineReadyState, CheckPipelineIsNotFinished, PipelineNotFinishedState))
	graph = append(graph, GraphEdge{PipelineReadyState, CheckPipelines, ReviewableState, NotCompliantState})
	graph = append(graph, SimpleEdge(ReviewableState, CheckNoActivity, AcceptedState))
	graph = append(graph, SimpleEdge(ReviewableState, CheckVoteOrThread, InReviewState))
	graph = append(graph, SimpleEdge(InReviewState, CheckAllThreadResolvedAndDevScoreNonStrict, AcceptableState))
	graph = append(graph, SimpleEdge(AcceptableState, CheckIsNotActiveAnymoreDeveloper, AcceptedState))
	// The other edges (Acceptable -> In Review, Accepted -> In Review, etc.) will never be trigger for now:
	// Homer will never reach the Acceptable/In Review states - votes or discussions will not meet the requirements
	return graph
}

// CheckStatusNotDecidable checks the MR status is considered as not usable
// any status other than CanBeMerged or CannotBeMerged is considered as a quantum state.
func CheckStatusNotDecidable(mr *MergeRequest) Check {
	return Check{
		"MR status should be decidable",
		mr.MergeStatus != MergeStatusCanBeMerged && mr.MergeStatus != MergeStatusCannotBeMerged,
		fmt.Sprintf("MR internal status is '%s'", mr.MergeStatus),
		nil,
	}
}

// CheckMergeable checks the MR status is considered mergeable
func CheckMergeable(mr *MergeRequest) Check {
	return Check{
		"MR should be considered mergeable by Gitlab",
		mr.MergeStatus == MergeStatusCanBeMerged,
		fmt.Sprintf("MR internal status is '%s'", mr.MergeStatus),
		nil,
	}
}

// CheckPipelineIsNotFinished checks last pipeline of MR is still pending/not finished
func CheckPipelineIsNotFinished(mr *MergeRequest) Check {
	description := "Last pipeline is not finished yet"
	headPipeline, err := mr.GetHeadPipeline()
	if err != nil {
		return Check{description, false, "", err}
	}
	if headPipeline == nil {
		return Check{description, false, "No pipeline has been found", nil}
	}
	return Check{
		description,
		headPipeline.Status == PipelineStatusCreated ||
			headPipeline.Status == PipelineStatusWaitingForResource ||
			headPipeline.Status == PipelineStatusPreparing ||
			headPipeline.Status == PipelineStatusPending ||
			headPipeline.Status == PipelineStatusRunning ||
			headPipeline.Status == PipelineStatusManual ||
			headPipeline.Status == PipelineStatusScheduled,
		fmt.Sprintf("Last Pipeline (%d) has status %s", headPipeline.ID, headPipeline.Status),
		nil,
	}
}

// CheckPipelines checks last pipeline of MR is successful
func CheckPipelines(mr *MergeRequest) Check {
	description := "Last pipeline should be successful"
	headPipeline, err := mr.GetHeadPipeline()
	if err != nil {
		return Check{description, false, "", err}
	}
	if headPipeline == nil {
		return Check{description, false, "No pipeline has been found", nil}
	}
	return Check{
		description,
		headPipeline.Status == PipelineStatusSuccess,
		fmt.Sprintf("Last Pipeline (%d) has status %s", headPipeline.ID, headPipeline.Status),
		nil,
	}
}

// CheckVoteOrThread checks if the MR review has started, aka some votes and/or
// some threads have been opened
func CheckVoteOrThread(mr *MergeRequest) Check {
	description := "MergeRequest should have at least one external review and/or vote"
	// strictly or not, we only need the number of votes here
	_, votes, _, err := mr.DevScore(false)
	if err != nil {
		return Check{description, false, "", err}
	}
	resolvableNotes, err := mr.GetAllNotes(true)
	if err != nil {
		return Check{description, false, "", err}
	}
	noteFound := false
	for _, note := range *resolvableNotes {
		if note.Author.ID != mr.Author.ID {
			noteFound = true
			break
		}
	}
	return Check{
		description,
		votes > 0 || noteFound,
		fmt.Sprintf("Votes: %d, Note found: %t", votes, noteFound),
		nil,
	}
}

// CheckNoActivity checks MR "full" inactivity: no vote, no discussion (opened or closed)
func CheckNoActivity(mr *MergeRequest) Check {
	threshold := time.Duration(mrInactiveHours) * time.Hour
	description := fmt.Sprintf("No activity on MR (no review, no vote) and last update is longer than %s", threshold)
	lastUpdate, err := mr.GetLatestVersionTime()
	if err != nil {
		return Check{description, false, "", err}
	}
	logMRVerbose(mr.IID, "Latest MR Version Time:", lastUpdate)
	timeSinceLastUpdate := time.Since(lastUpdate)
	if timeSinceLastUpdate < threshold {
		return Check{description, false, fmt.Sprintf("Last update is %s", timeSinceLastUpdate.Round(time.Minute)), nil}
	}
	_, votes, _, err := mr.DevScore(false)
	if err != nil {
		return Check{description, false, "", err}
	}
	if votes > 0 {
		return Check{description, false, fmt.Sprintf("Dev votes have been found (%d)", votes), nil}
	}
	resolvableNotes, err := mr.GetAllNotes(true)
	if err != nil {
		return Check{description, false, "", err}
	}
	actualNotes := 0
	for _, note := range *resolvableNotes {
		if note.Author.ID == mr.Author.ID && note.Resolved {
			// threads the author has opened do not count, I suppose ?
			continue
		}
		actualNotes++
	}
	if actualNotes > 0 {
		return Check{description, false, fmt.Sprintf("Discussion threads have been found (%d)", actualNotes), nil}
	}
	return Check{description, true, fmt.Sprintf("No developer vote or thread has been found, and time since last update is %s", timeSinceLastUpdate.Round(time.Minute)), nil}
}

// CheckAllThreadResolvedAndDevScoreStrict checks if all threads have been resolved and score is strictly positive
func CheckAllThreadResolvedAndDevScoreStrict(mr *MergeRequest) Check {
	description := "All threads should be resolved, have votes and score > 0"
	check, _ := CheckAllThreadResolved(mr)
	if check.Error != nil || !check.Success {
		return check
	}
	score, votes, _, err := mr.DevScore(true)
	if err != nil {
		return Check{description, false, "", err}
	}
	if votes == 0 {
		return Check{description, false, "No Developer vote has been found", nil}
	}
	if score <= 0 {
		return Check{description, false, fmt.Sprintf("Score is not strictly positive (%d)", score), nil}
	}
	return Check{description, true, fmt.Sprintf("All threads seem resolved, developer votes (%d) found and score is strictly positive (%d)", votes, score), nil}
}

// CheckAllThreadResolvedAndDevScoreNonStrict checks if all threads have been resolved and score is positive
func CheckAllThreadResolvedAndDevScoreNonStrict(mr *MergeRequest) Check {
	description := "All threads should be resolved, and score >= 0"
	check, _ := CheckAllThreadResolved(mr)
	if check.Error != nil || !check.Success {
		return check
	}
	score, _, _, err := mr.DevScore(false)
	if err != nil {
		return Check{description, false, "", err}
	}
	if score < 0 {
		return Check{description, false, fmt.Sprintf("Score is not positive (%d)", score), nil}
	}
	return Check{description, true, fmt.Sprintf("All threads seem resolved, and developers score is positive (%d)", score), nil}
}

// CheckIsNotActiveAnymoreUser checks if a User MR has been inactive for a certain time
func CheckIsNotActiveAnymoreUser(mr *MergeRequest) Check {
	return CheckIsNotActiveAnymore(mr, mrOtherDiscussionHours, mrOtherScoreHours, true)
}

// CheckIsNotActiveAnymoreDeveloper checks if a Developer MR has been inactive for a certain time
func CheckIsNotActiveAnymoreDeveloper(mr *MergeRequest) Check {
	return CheckIsNotActiveAnymore(mr, mrDevDiscussionHours, mrDevScoreHours, false)
}

// CheckIsNotActiveAnymore checks if a MR has been inactiv for a certain time
func CheckIsNotActiveAnymore(mr *MergeRequest, discussionHoursThreshold int64, scoreHoursThreshold int64, strictly bool) Check {
	description := fmt.Sprintf("MergeRequest should have no activity (threads/votes) for (%dh/%dh)", discussionHoursThreshold, scoreHoursThreshold)
	check, lastResolved := CheckAllThreadResolved(mr)
	if check.Error != nil || !check.Success {
		return check
	}
	discussionThreshold := time.Duration(discussionHoursThreshold) * time.Hour
	// lastResolved can be zero time, but this has no consequence here
	if since := time.Since(lastResolved); !lastResolved.IsZero() && since < discussionThreshold {
		return Check{description, false, fmt.Sprintf("Discussions have been resolved, but too soon (%fh)", since.Hours()), nil}
	}
	_, _, lastSignChange, err := mr.DevScore(strictly)
	if err != nil {
		return Check{description, false, "", err}
	}
	voteThreshold := time.Duration(scoreHoursThreshold) * time.Hour
	// lastSignChange can be zero time, in case of a Developer MR
	if since := time.Since(lastSignChange); since < voteThreshold {
		return Check{description, false, fmt.Sprintf("Score is strictly positive, but too soon (%fh)", since.Hours()), nil}
	}
	return Check{description, true, "All threads seem resolved, developer votes is strictly positive, both for enough time", nil}
}

// CheckAllThreadResolved checks all threads in MR have been resolved
// Returns check, and last resolved time (zero if none)
func CheckAllThreadResolved(mr *MergeRequest) (Check, time.Time) {
	description := fmt.Sprintf("All Threads should be resolved")
	notes, err := mr.GetAllNotes(true)
	if err != nil {
		return Check{description, false, "", err}, time.Time{}
	}
	logMRVerbose(mr.IID, "Found ", len(*notes), " resolvable threads")
	lastResolved := time.Time{}
	for _, note := range *notes {
		if !note.Resolved {
			return Check{description, false, fmt.Sprintf("A thread is still [unresolved](#note_%d)", note.ID), nil}, time.Time{}
		}
		//if note.Author.ID != mr.Author.ID && note.ResolvedBy != nil && note.ResolvedBy.ID == mr.Author.ID {
		//	// NOTE: should we do something else ? A "Invalid" label on the MR ?
		//	return Check{description, false, fmt.Sprintf("A thread not started by the MR Author has been closed by the MR Author: [thread](#note_%d)", note.ID), nil}, time.Time{}
		//}
		// resolved + legit => take updatetime into account
		if note.UpdatedAt.After(lastResolved) {
			lastResolved = note.UpdatedAt
		}
	}
	return Check{description, true, "No unresolved thread has been found", nil}, lastResolved
}
